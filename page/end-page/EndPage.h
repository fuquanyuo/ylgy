#pragma once
#ifndef __END_PAGE__
#define __END_PAGE__

void endPage();
void createWiningPicture();//游戏胜利画面
void createFailedPicture();//游戏失败画面
void drawWiningPicture();
void drawFailedPicture();
void drawExitGameWining();
void drawExitGameFailed();
#endif